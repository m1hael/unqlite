#ifndef _UNAME_H_
#define _UNAME_H_

struct utsname {
  char  sysname[];
  char  nodename[];
  char  release[];
  char  version[];
  char  machine[10]; // flexible array member as last member not allowed on IBM i
};

int uname(struct utsname *name);

int uname(struct utsname *name) {
  strcpy(name->sysname, "OS400");
  strcpy(name->nodename, "localhost");
  strcpy(name->release, "3");
  strcpy(name->version, "7");
  strcpy(name->machine, "localhost");
  
  return 0;
}

#endif