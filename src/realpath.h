#ifndef _REALPATH_H_
#define _REALPATH_H_

char *realpath(const char *file_name, char *resolved_name);

char *realpath(const char *file_name, char *resolved_name) {
    strcpy(resolved_name, file_name);

    return resolved_name;
}

#endif
